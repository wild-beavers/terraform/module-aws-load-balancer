####
# Variables
####

variable "application_waf_attachment_enabled" {
  type        = bool
  description = "Whether to associate the ALB on with the WAF “var.application_waf_arn” or not."
  default     = false
}

variable "application_waf_arn" {
  type        = string
  description = "ARN of the WAF to associate with the ALB"
  default     = null
}

####
# Resources & data
####

resource "aws_wafv2_web_acl_association" "this" {
  for_each = var.application_waf_attachment_enabled ? { 0 = 0 } : {}

  resource_arn = aws_lb.this["0"].arn
  web_acl_arn  = var.application_waf_arn

  lifecycle {
    precondition {
      condition     = !(!local.is_application_load_balancer && var.application_waf_attachment_enabled)
      error_message = "A WAF can only be attached to an application load balancer."
    }
  }
}

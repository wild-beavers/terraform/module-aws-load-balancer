## 2.0.3

- fix: nothing new, just for pipeline re run.

## 2.0.2

- fix: `precomputed_aws_lb_target_groups` should be within `precomputed`.

## 2.0.1

- fix: `precomputed_aws_lb` should be named `precomputed`.

## 2.0.0

- chore: bump pre-commit hooks
- refactor: makes `var.listener`, `var.alb_listeners`, `var.alb_listener_rules`, `var.target_groups` non-nullable
- refactor: (BREAKING) changes outputs to new standards:
   - `load_balancer.xxx` => `aws_lb.xxx`
   - `listeners[x].yyy` => `aws_lb_listeners[x].yyy`
   - `listeners[x].rules[y].zzz` => `aws_lb_listener_rules[y].zzz`
   - `target_groups[x].yyy` => `aws_lb_target_group[x].yyy`
- fix: makes `tag.origin` non-overridable to avoid upper module origins to erase this one

## 1.1.1

- fix: drops invalid headers for ALB by default, as this module should provide sane defaults

## 1.1.0

- feat: adds possibility to use an external target group to listeners
- chore: bump pre-commit hooks
- test: rename application test to default to comply with CI jobs

## 1.0.3

- fix: validation issue on listener rules
- fix: wrong target group attribute on single target group forward

## 1.0.2

- fix: deprecates “var.application_enable_xff_client_port” replaced by “var.application_xff_client_port_enabled” to comply with naming standards
- chore: bump pre-commit hooks

## 1.0.1

- fix: missing tag `Name` on target groups
- fix: wrong merging order for `var.tags`

## 1.0.0

- feat: init

## 0.0.0

- tech: initial version

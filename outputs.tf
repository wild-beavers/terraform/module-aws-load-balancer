output "precomputed" {
  value = merge(
    {
      aws_lb = {
        name = var.name
      }
    },
    length(var.target_groups) > 0 ? {
      aws_lb_target_groups = { for k, v in var.target_groups :
        k => { name = v.name } if !v.is_name_prefix
      }
    } : null
  )
}

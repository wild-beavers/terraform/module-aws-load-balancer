####
# Variables
####

variable "target_group_tags" {
  type        = map(string)
  description = "Tags to applied and merge with all target groups."
  default     = null
}

variable "target_groups" {
  type = map(object({
    name                               = string
    type                               = string
    is_name_prefix                     = optional(bool, false)
    connection_termination             = optional(bool)
    deregistration_delay               = optional(number)
    lambda_multi_value_headers_enabled = optional(bool)
    load_balancing_algorithm_type      = optional(string)
    load_balancing_cross_zone_enabled  = optional(bool)
    port                               = optional(number)
    preserve_client_ip                 = optional(bool)
    protocol_version                   = optional(string)
    protocol                           = optional(string)
    proxy_protocol_v2                  = optional(bool)
    slow_start                         = optional(number)

    health_check_enabled             = optional(bool, true)
    health_check_healthy_threshold   = optional(number)
    health_check_interval            = optional(number)
    health_check_matcher             = optional(list(string))
    health_check_path                = optional(string)
    health_check_port                = optional(number)
    health_check_protocol            = optional(string)
    health_check_timeout             = optional(number)
    health_check_unhealthy_threshold = optional(number)

    stickiness_type            = optional(string)
    stickiness_cookie_duration = optional(number)
    stickiness_cookie_name     = optional(string)

    gateway_target_failover_enabled           = optional(bool, false)
    gateway_target_failover_on_deregistration = optional(string)
    gateway_target_failover_on_unhealthy      = optional(string)

    ip_address_type = optional(string)

    attachments = optional(map(object({
      id                = string
      availability_zone = optional(string)
      port              = optional(number)
    })))
    tags = optional(map(string), {})

  }))
  description = <<DOCUMENTATION
Target Groups.
Keys are free values.
- name (required, string)                              : Name of the target group.
- type (required, string)                              : Type of target that you must specify when registering targets with this target group. Must be on of “instance”, “ip”, “alb” or “lambda”.
- is_name_prefix (optional, bool, false)               : Whether to creates a unique name beginning with the specified prefix in “name” or not.
- connection_termination (optional, bool)              : Whether to terminate connections at the end of the deregistration timeout on Network Load Balancers or not.
- deregistration_delay (optional, number)              : Amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused. The range is 0-3600 seconds.
- lambda_multi_value_headers_enabled  (optional, bool) : Whether the request and response headers exchanged between the load balancer and the Lambda function include arrays of values or strings. Only applies when target_type is lambda.
- load_balancing_algorithm_type (optional, string)     : Determines how the load balancer selects targets when routing requests. Only applicable for Application Load Balancer Target Groups. The value is “round_robin” or “least_outstanding_requests”.
- load_balancing_cross_zone_enabled (optional, bool)   : Whether to enable cross zone load balancing or not. If not set, it takes the load balancer configuration.
- port (optional, number)                              : Port on which targets receive traffic, unless overridden when registering a specific target. Required when type is one of “instance”, “ip” or “alb”.
- preserve_client_ip (optional, bool)                  : Whether to preserve client IP or not. Only applicable to network load balancer
- protocol_version (optional, string)                  : Only applicable when protocol is “HTTP” or “HTTPS”. The protocol version. Specify “GRPC” to send requests to targets using gRPC. Specify “HTTP2” to send requests to targets using HTTP/2.
- protocol (optional, string)                          : Protocol to use for routing traffic to the targets. Must be one of “GENEVE”, “HTTP”, “HTTPS”, “TCP”, “TCP_UDP”, “TLS”, or “UDP”. Required when type is one of “instance”, “ip” or “alb”.
- proxy_protocol_v2 (optional, bool)                   : Whether to enable support for proxy protocol v2 on Network Load Balancers or not.
- slow_start (optional, number))                       : Amount time for targets to warm up before the load balancer sends them a full share of requests. The range is 30-900 seconds or 0 to disable. This apply only on application load balancer. This timer is independent with AutoScaling grace period.
- ip_address_type (optional, string)                   : The type of IP addresses used by the target group, only supported when target type is set to ip. Possible values are “ipv4“ or “ipv6”.
- tags (optional, map(string), {})                     : Tags to be merge with this target group.

- health_check_enabled (optional, bool, true)         : Whether to enable health check or not.
- health_check_healthy_threshold (optional, number)   : Number of consecutive health check successes required before considering a target healthy. The range is 2-10.
- health_check_interval (optional, number)            : Approximate amount of time, in seconds, between health checks of an individual target. The range is 5-300.
- health_check_matcher (optional, list(string))       : Response codes to use when checking for a healthy responses from a target. You can specify multiple values (for example, "200,202" for HTTP(s) or "0,12" for GRPC) or a range of values (for example, "200-299" or "0-99"). Required for HTTP/HTTPS/GRPC ALB. Only applies to Application Load Balancers (i.e., HTTP/HTTPS/GRPC) not Network Load Balancers (i.e., TCP)
- health_check_path (optional, string)                : Destination for the health check request. Required for HTTP/HTTPS ALB and HTTP NLB. Only applies to HTTP/HTTPS.
- health_check_port (optional, number)                : The port the load balancer uses when performing health checks on targets.
- health_check_protocol (optional, string)            : Protocol the load balancer uses when performing health checks on targets. Must be either “TCP”, “HTTP”, or “HTTPS”. The “TCP” protocol is not supported for health checks if the protocol of the target group is “HTTP” or “HTTPS”.
- health_check_timeout (optional, number)             : Amount of time, in seconds, during which no response from a target means a failed health check. The range is 2–120 seconds.
- health_check_unhealthy_threshold (optional, number) : Number of consecutive health check failures required before considering a target unhealthy. The range is 2-10.

- stickiness_type (optional, string)            : The type of sticky sessions. The only current possible values are “lb_cookie”, “app_cookie” for ALBs, “source_ip” for NLBs, and “source_ip_dest_ip”, “source_ip_dest_ip_proto” for GWLBs.
- stickiness_cookie_duration (optional, number) : Only used when the type is “lb_cookie”. The time period, in seconds, during which requests from a client should be routed to the same target. After this time period expires, the load balancer-generated cookie is considered stale. The range is 1 second to 1 week (604800 seconds).
- stickiness_cookie_name (optional, string)     : Name of the application based cookie. AWSALB, AWSALBAPP, and AWSALBTG prefixes are reserved and cannot be used. Only needed when type is “app_cookie”.

- gateway_target_failover_enabled (optional, bool, false)     : Whether to enabled the gateway target failover or not.
- gateway_target_failover_on_deregistration (optional,string) : Indicates how the GWLB handles existing flows when a target is deregistered. Possible values are “rebalance” and “no_rebalance”. Must match the attribute value set for on_unhealthy.
- gateway_target_failover_on_unhealthy (optional, string)     : Indicates how the GWLB handles existing flows when a target is unhealthy. Possible values are “rebalance” and “no_rebalance”. Must match the attribute value set for on_deregistration.

- attachments (optional, map(object(see below))): Target to attached to target groups.
  Keys are free values.
  - id (required, string)               : The ID of the target. This is the Instance ID for an instance. If the target type is ip, specify an IP address. If the target type is lambda, specify the Lambda function ARN. If the target type is alb, specify the ALB ARN.
  - availability_zone (optional, string): The Availability Zone where the IP address of the target is to be registered. If the private IP address is outside of the VPC scope, this value must be set to all.
  - port (optional, number)             : The port on which targets receive traffic
DOCUMENTATION
  default     = {}
  nullable    = false

  validation {
    condition = alltrue([for k, v in var.target_groups : (
      can(regex("^[^\\-][\\w\\-]{1,31}[\\w]$", v.name)) &&
      (v.deregistration_delay == null || (coalesce(v.deregistration_delay, -1) >= 0 && coalesce(v.deregistration_delay, 3601) <= 3600)) &&
      (v.load_balancing_algorithm_type == null || contains(["round_robin", "least_outstanding_requests"], coalesce(v.load_balancing_algorithm_type, "undefined"))) &&
      (v.port == null || (coalesce(v.port, -1) > 0 && coalesce(v.port, 65536) <= 65535)) &&
      (v.protocol_version == null || contains(["GRPC", "HTTP1", "HTTP2"], coalesce(v.protocol_version, "undefined"))) &&
      (v.protocol == null || contains(["GENEVE", "HTTP", "HTTPS", "TCP", "TCP_UDP", "TLS", "UDP"], coalesce(v.protocol, "undefined"))) &&
      (v.slow_start == null || coalesce(v.slow_start, -1) == 0 || (coalesce(v.slow_start, -1) >= 30 && coalesce(v.slow_start, 901) <= 900)) &&
      contains(["instance", "ip", "alb", "lambda"], v.type) &&
      (v.ip_address_type == null || contains(["ipv4", "ipv6"], coalesce(v.ip_address_type, "undefined"))) &&
      (v.health_check_healthy_threshold == null || (coalesce(v.health_check_healthy_threshold, -1) >= 2 && coalesce(v.health_check_healthy_threshold, 11) <= 10)) &&
      (v.health_check_interval == null || (coalesce(v.health_check_interval, -1) >= 5 && coalesce(v.health_check_interval, 301) <= 300)) &&
      (v.health_check_port == null || (coalesce(v.health_check_port, -1) > 0 || coalesce(v.health_check_port, 65536) <= 65535)) &&
      (v.health_check_protocol == null || contains(["TCP", "HTTP", "HTTPS"], coalesce(v.health_check_protocol, "undefined"))) &&
      (v.health_check_timeout == null || (coalesce(v.health_check_timeout, -1) >= 2 && coalesce(v.health_check_timeout, 121) <= 120)) &&
      (v.health_check_unhealthy_threshold == null || (coalesce(v.health_check_unhealthy_threshold, -1) >= 2 && coalesce(v.health_check_unhealthy_threshold, 11) <= 10)) &&
      (v.stickiness_cookie_duration == null || (coalesce(v.stickiness_cookie_duration, -1) >= 1 || coalesce(v.stickiness_cookie_duration, 604801) <= 604800)) &&
      (v.stickiness_type == null || contains(["lb_cookie", "app_cookie", "source_ip", "source_ip_dest_ip", "source_ip_dest_ip_proto"], coalesce(v.stickiness_type, "undefined"))) &&
      (v.gateway_target_failover_on_deregistration == null || contains(["rebalance", "no_rebalance"], coalesce(v.gateway_target_failover_on_deregistration, "undefined"))) &&
      (v.gateway_target_failover_on_unhealthy == null || contains(["rebalance", "no_rebalance"], coalesce(v.gateway_target_failover_on_unhealthy, "undefined"))) &&
      (v.attachments == null || alltrue([for k, attachment in coalesce(v.attachments, {}) : (
        (
          can(regex("^i\\-([a-f0-9]{8}|[a-f0-9]{17})$", attachment.id)) ||
          can(cidrhost(format("%s/31", attachment.id), 1)) ||
          can(regex("^arn:(?:aws|aws-cn|aws-us-gov):lambda:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:function:[a-zA-Z0-9\\-]{1,64}$", attachment.id)) ||
          can(regex("^arn:(?:aws|aws-cn|aws-us-gov):elasticloadbalancing:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:loadbalancer/app/[^\\-][\\w\\-]{1,31}[\\w]?/[a-z0-9]{16}$", attachment.id))
        ) &&
        (attachment.availability_zone == null || attachment.availability_zone == "all" || can(regex("^[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}[a-z]$", coalesce(attachment.availability_zone, "undefined")))) &&
        (attachment.port == null || (coalesce(attachment.port, -1) > 0 && (coalesce(attachment.port, 65536) <= 65535)))
      )]))
    )])
    error_message = "One of more elements of “var.target_groups” are not valid."
  }
}

####
# Resources & data
####

locals {
  attachments = merge([
    for k, v in var.target_groups : {
      for key, value in coalesce(v.attachments, {}) : (format("%s_%s", k, key)) => {
        target_group_arn  = aws_lb_target_group.this[k].arn
        is_lambda_type    = lookup(var.target_groups, k, null).type == "lambda"
        id                = value.id
        availability_zone = value.availability_zone
        port              = value.port
      }
    }
  ]...)
}

resource "aws_lb_target_group" "this" {
  for_each = var.target_groups

  name        = !each.value.is_name_prefix ? each.value.name : null
  name_prefix = each.value.is_name_prefix ? each.value.name : null

  connection_termination             = each.value.connection_termination
  deregistration_delay               = each.value.deregistration_delay
  lambda_multi_value_headers_enabled = each.value.lambda_multi_value_headers_enabled
  load_balancing_algorithm_type      = each.value.load_balancing_algorithm_type
  load_balancing_cross_zone_enabled  = each.value.load_balancing_cross_zone_enabled
  port                               = each.value.protocol == "GENEVE" ? 6081 : each.value.port
  preserve_client_ip                 = each.value.preserve_client_ip
  protocol_version                   = each.value.protocol_version
  protocol                           = each.value.protocol
  proxy_protocol_v2                  = each.value.proxy_protocol_v2
  ip_address_type                    = each.value.ip_address_type
  slow_start                         = each.value.slow_start
  target_type                        = each.value.type
  vpc_id                             = var.vpc_id

  dynamic "health_check" {
    for_each = each.value.health_check_enabled == true ? { 0 = 0 } : {}

    content {
      healthy_threshold   = each.value.health_check_healthy_threshold
      interval            = each.value.health_check_interval
      matcher             = join(",", coalesce(each.value.health_check_matcher, []))
      path                = each.value.health_check_path
      port                = each.value.health_check_port
      protocol            = each.value.health_check_protocol
      timeout             = each.value.health_check_timeout
      unhealthy_threshold = each.value.health_check_unhealthy_threshold
    }
  }

  dynamic "stickiness" {
    for_each = contains(["lb_cookie", "app_cookie", "source_ip", "source_ip_dest_ip", "source_ip_dest_ip_proto"], coalesce(each.value.stickiness_type, "undefined")) ? { 0 = 0 } : {}

    content {
      type            = each.value.stickiness_type
      cookie_duration = each.value.stickiness_cookie_duration
      cookie_name     = each.value.stickiness_cookie_name
    }
  }

  dynamic "target_failover" {
    for_each = each.value.gateway_target_failover_enabled ? { 0 = 0 } : {}
    content {
      on_deregistration = each.value.gateway_target_failover_on_deregistration
      on_unhealthy      = each.value.gateway_target_failover_on_unhealthy
    }
  }

  tags = merge(
    local.tags,
    var.target_group_tags,
    each.value.tags,
    {
      Name = each.value.name
    }
  )

  lifecycle {
    precondition {
      condition     = !(local.is_gateway_load_balancer && length(var.target_groups) > 1)
      error_message = "You can only have one target group with gateway load balancer"
    }
  }
}

resource "aws_lb_target_group_attachment" "this" {
  for_each = coalesce(local.attachments, {})

  target_group_arn  = each.value.target_group_arn
  target_id         = each.value.id
  availability_zone = each.value.availability_zone
  port              = each.value.port

  depends_on = [aws_lambda_permission.this]
}

####
# Lambda permissions
####

locals {
  lambda_permissions = {
    for k, v in coalesce(local.attachments, {}) : k => v if v.is_lambda_type
  }
}

resource "aws_lambda_permission" "this" {
  for_each = coalesce(local.lambda_permissions, {})

  function_name = split(":", each.value.id)[6]

  statement_id = "AllowExecutionFromLb"
  action       = "lambda:InvokeFunction"
  principal    = "elasticloadbalancing.amazonaws.com"
  source_arn   = each.value.target_group_arn
}

####
# Outputs
####

output "aws_lb_target_groups" {
  // As of 2024-06-10, it seems "load_balancer_arns" is not idempotent, thus removed from outputs
  value = var.target_groups != {} ? { for k, v in aws_lb_target_group.this :
    k => { for i, j in v : i => j if i != "load_balancer_arns" }
  } : null
}

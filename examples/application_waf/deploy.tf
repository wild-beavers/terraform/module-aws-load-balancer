locals {
  prefix               = "${random_string.start_letter.result}${random_string.this.result}"
  resource_name_prefix = "${local.prefix}tftest-lb-module-"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_availability_zones" "default" {
  state = "available"
}


data "aws_subnets" "default" {
  count = length(data.aws_availability_zones.default.names)

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "availability-zone"
    values = [element(data.aws_availability_zones.default.names, count.index)]
  }
}

resource "aws_security_group" "test" {
  name = "${local.resource_name_prefix}sg"
}

resource "aws_wafv2_web_acl" "test" {
  name  = "${local.resource_name_prefix}waf"
  scope = "REGIONAL"

  default_action {
    allow {}
  }

  visibility_config {
    cloudwatch_metrics_enabled = false
    metric_name                = "never-use"
    sampled_requests_enabled   = false
  }
}

module "application_waf" {
  source = "../../"

  name = "${local.resource_name_prefix}waf-alb"

  subnets = {
    0 = {
      subnet_id = data.aws_subnets.default[0].ids[0]
    }
    1 = {
      subnet_id = data.aws_subnets.default[1].ids[0]
    }
  }

  application_drop_invalid_header_fields                   = false
  application_xff_client_port_enabled                      = true
  application_http2_enabled                                = true
  application_idle_timeout                                 = 60
  application_preserve_host_header                         = false
  security_groups                                          = [aws_security_group.test.id]
  application_tls_version_and_cipher_suite_headers_enabled = true
  application_waf_fail_open_enabled                        = true
  application_xff_header_processing_mode                   = "append"

  #  type     = "application"
  internal = true
  vpc_id   = data.aws_vpc.default.id

  application_waf_attachment_enabled = true
  application_waf_arn                = aws_wafv2_web_acl.test.arn
}

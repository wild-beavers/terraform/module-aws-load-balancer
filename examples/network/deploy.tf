locals {
  prefix               = "${random_string.start_letter.result}${random_string.this.result}"
  resource_name_prefix = "${local.prefix}tftest-lb-module-"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_availability_zones" "default" {
  state = "available"
}


data "aws_subnets" "default" {
  count = length(data.aws_availability_zones.default.names)

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "availability-zone"
    values = [element(data.aws_availability_zones.default.names, count.index)]
  }
}

resource "tls_private_key" "test" {
  algorithm = "RSA"
}

resource "tls_self_signed_cert" "test" {
  private_key_pem = tls_private_key.test.private_key_pem

  validity_period_hours = 2

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]

  dns_names = ["example.wildbeavers"]

  subject {
    common_name  = "example.wildbeavers"
    organization = "${local.prefix}, tftest, wildbeaver"
  }
}

resource "aws_acm_certificate" "test" {
  certificate_body = tls_self_signed_cert.test.cert_pem
  private_key      = tls_private_key.test.private_key_pem

  tags = merge(
    {
      Name = "${local.resource_name_prefix}certificate"
    }
  )
}

resource "aws_lb_target_group" "example" {
  name        = "${local.resource_name_prefix}external"
  target_type = "ip"
  port        = "80"
  protocol    = "TCP"
  vpc_id      = data.aws_vpc.default.id
  tags = {
    Name = "${local.resource_name_prefix}external"
  }
}

module "network" {
  source = "../../"

  name = "${local.resource_name_prefix}network"

  subnets = {
    0 = {
      subnet_id = data.aws_subnets.default[0].ids[0]
    }
    1 = {
      subnet_id = data.aws_subnets.default[1].ids[0]
    }
  }
  type     = "network"
  internal = true

  vpc_id = data.aws_vpc.default.id

  target_groups = {
    ip_tcp = {
      name                   = "${local.resource_name_prefix}ip-tcp"
      port                   = 80
      protocol               = "TCP"
      connection_termination = true
      deregistration_delay   = 50
      type                   = "ip"
    }
    instance_tcp = {
      name     = "${local.resource_name_prefix}inst-tcp"
      port     = 8080
      protocol = "TCP"
      type     = "instance"
    }
    alb_tcp = {
      name                 = "${local.resource_name_prefix}alb-tcp"
      port                 = 8081
      protocol             = "TCP"
      type                 = "alb"
      health_check_enabled = false
    }
    ip_tls = {
      name                              = "${local.resource_name_prefix}ip-tls"
      port                              = 8443
      protocol                          = "TLS"
      type                              = "ip"
      load_balancing_cross_zone_enabled = false
      preserve_client_ip                = false

      health_check_enabled             = true
      health_check_healthy_threshold   = 3
      health_check_interval            = 60
      health_check_matcher             = ["200", "201"]
      health_check_path                = "/healthcheck"
      health_check_port                = "8087"
      health_check_protocol            = "HTTPS"
      health_check_timeout             = "10"
      health_check_unhealthy_threshold = 6
    }
    ip_tcp_udp = {
      name                   = "${local.resource_name_prefix}ip-udp-tcp"
      port                   = 1000
      protocol               = "TCP_UDP"
      type                   = "ip"
      stickiness_type        = "source_ip"
      connection_termination = true
      ip_address_type        = "ipv4"
    }
    instance_udp = {
      name                   = "${local.resource_name_prefix}inst-udp"
      port                   = 51069
      protocol               = "UDP"
      connection_termination = true
      type                   = "instance"
    }
  }

  listeners = {
    0 = {
      target_group_key = "ip_tcp"
      port             = 80
      protocol         = "TCP"
    }

    1 = {
      target_group_key = "ip_tls"

      alpn_policy     = "HTTP2Preferred"
      certificate_arn = aws_acm_certificate.test.arn
      ssl_policy      = "ELBSecurityPolicy-TLS13-1-3-2021-06"
      port            = 443
      protocol        = "TLS"
    }

    external = {
      target_group_arn = aws_lb_target_group.example.arn

      port     = 8080
      protocol = "TCP"
    }
  }
}

locals {
  prefix               = "${random_string.start_letter.result}${random_string.this.result}"
  resource_name_prefix = "${local.prefix}tftest-lb-module-"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_availability_zones" "default" {
  state = "available"
}


data "aws_subnets" "default" {
  count = length(data.aws_availability_zones.default.names)

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "availability-zone"
    values = [element(data.aws_availability_zones.default.names, count.index)]
  }
}

data "aws_ssm_parameter" "ami_ubuntu" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

resource "aws_instance" "test" {
  ami           = data.aws_ssm_parameter.ami_ubuntu.value
  instance_type = "t3a.nano"
}

data "aws_iam_policy_document" "lambda" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "lambda" {
  name               = "${local.resource_name_prefix}lambda"
  assume_role_policy = data.aws_iam_policy_document.lambda.json

  tags = merge(
    {
      Name = "${local.resource_name_prefix}lambda"
    }
  )
}

data "archive_file" "lambda" {
  type = "zip"

  source_content          = file("${path.root}/files/lambda.js")
  source_content_filename = "index.js"
  output_path             = "${path.root}/files/lambda_${sha256(file("${path.root}/files/lambda.js"))}_js.zip"
}

resource "aws_lambda_function" "test" {
  function_name = "${local.resource_name_prefix}lambda"
  role          = aws_iam_role.lambda.arn

  filename = data.archive_file.lambda.output_path
  runtime  = "nodejs18.x"
  handler  = "module.exports.hello"
}

resource "tls_private_key" "test" {
  algorithm = "RSA"
}

resource "tls_self_signed_cert" "test" {
  private_key_pem = tls_private_key.test.private_key_pem

  validity_period_hours = 2

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]

  dns_names = ["example.wildbeavers"]

  subject {
    common_name  = "example.wildbeavers"
    organization = "${local.prefix}, tftest, wildbeaver"
  }
}

resource "aws_acm_certificate" "test" {
  certificate_body = tls_self_signed_cert.test.cert_pem
  private_key      = tls_private_key.test.private_key_pem

  tags = merge(
    {
      Name = "${local.resource_name_prefix}certificate"
    }
  )
}

resource "aws_lb_target_group" "example" {
  name        = "${local.resource_name_prefix}external"
  target_type = "ip"
  port        = "80"
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.default.id
  tags = {
    Name = "${local.resource_name_prefix}external"
  }
}

module "application" {
  source = "../../"

  name = "${local.resource_name_prefix}alb"

  subnets = {
    0 = {
      subnet_id = data.aws_subnets.default[0].ids[0]
    }
    1 = {
      subnet_id = data.aws_subnets.default[1].ids[0]
    }
  }

  target_groups = {
    first_http = {
      name            = "${local.resource_name_prefix}http-ip"
      port            = 80
      protocol        = "HTTP"
      type            = "ip"
      slow_start      = 600
      stickiness_type = "lb_cookie"
      attachments = {
        server-1 = {
          id                = "172.30.0.1"
          port              = 8081
          availability_zone = "us-east-1a"
        }
        within-vpc = {
          id = "172.31.0.10"
        }
      }
    }
    second_https = {
      name             = "${local.resource_name_prefix}http-inst"
      port             = 8080
      protocol         = "HTTPS"
      type             = "instance"
      protocol_version = "HTTP2"
      attachments = {
        instance = {
          id = aws_instance.test.id
        }
      }
    }
    lambda = {
      name                  = "${local.resource_name_prefix}lambda"
      type                  = "lambda"
      health_check_interval = 31
      attachments = {
        lambda = {
          id = aws_lambda_function.test.arn
        }
      }
    }
    lambda2 = {
      name                               = "${local.resource_name_prefix}lambda2"
      type                               = "lambda"
      health_check_interval              = 120
      lambda_multi_value_headers_enabled = true
    }
    stickiness = {
      name                       = "${local.resource_name_prefix}sticky"
      port                       = 9443
      protocol                   = "HTTPS"
      type                       = "ip"
      protocol_version           = "HTTP2"
      stickiness_type            = "lb_cookie"
      stickiness_cookie_duration = 42
    }
    stickiness2 = {
      name                          = "${local.resource_name_prefix}sticky2"
      port                          = 9080
      protocol                      = "HTTP"
      type                          = "instance"
      load_balancing_algorithm_type = "least_outstanding_requests"
      stickiness_type               = "app_cookie"
      stickiness_cookie_name        = "eatme"
    }
  }

  alb_listeners = {
    http_to_https = {
      port                 = 8080
      action_type          = "redirect"
      protocol             = "HTTP"
      redirect_status_code = "HTTP_301"
      redirect_port        = "8443"
      redirect_protocol    = "HTTPS"
    }

    https_tls13_only = {
      port        = 8443
      action_type = "forward"
      protocol    = "HTTPS"
      target_groups = [
        {
          key = "stickiness"
        },
        {
          key    = "second_https"
          weight = "50"
        },
      ]
      stickiness_duration = 60

      certificate_arn = aws_acm_certificate.test.arn
      ssl_policy      = "ELBSecurityPolicy-TLS13-1-3-2021-06"
    }

    lambda = {
      port        = 8081
      action_type = "forward"
      protocol    = "HTTP"
      target_groups = [
        {
          key = "lambda"
        }
      ]
    }

    external = {
      port        = 80
      action_type = "forward"
      protocol    = "HTTP"
      target_groups = [
        {
          arn = aws_lb_target_group.example.arn
        }
      ]
    }
  }

  alb_listener_rules = {
    404 = {
      listener_key = "https_tls13_only"

      action_type = "fixed-response"

      fixed_response_content_type = "text/plain"
      fixed_response_message_body = "Psst Roger! You're totally lost!"
      fixed_response_status_code  = 404

      path_patterns = ["/4xx", "/5xx"]
    }

    external_forward = {
      listener_key = "https_tls13_only"

      action_type = "forward"
      protocol    = "HTTP"
      priority    = "2"

      target_groups = [
        {
          arn = aws_lb_target_group.example.arn
        }
      ]

      path_patterns = ["/3xx"]
    }
  }

  type     = "application"
  internal = false
  vpc_id   = data.aws_vpc.default.id
}

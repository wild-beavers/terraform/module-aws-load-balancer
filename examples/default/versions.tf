terraform {
  required_version = ">= 1.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.13"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">= 4.0.4"
    }
    archive = {
      source  = "hashicorp/archive"
      version = ">= 2.4.0"
    }
  }
}

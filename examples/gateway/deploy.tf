locals {
  prefix               = "${random_string.start_letter.result}${random_string.this.result}"
  resource_name_prefix = "${local.prefix}tftest-lb-module-"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_availability_zones" "default" {
  state = "available"
}


data "aws_subnets" "default" {
  count = length(data.aws_availability_zones.default.names)

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "availability-zone"
    values = [element(data.aws_availability_zones.default.names, count.index)]
  }
}

module "gateway" {
  source = "../../"

  name = "${local.resource_name_prefix}gateway"
  subnets = {
    0 = {
      subnet_id = data.aws_subnets.default[0].ids[0]
    }
    1 = {
      subnet_id = data.aws_subnets.default[1].ids[0]
    }
  }
  type     = "gateway"
  internal = false

  target_groups = {
    instance = {
      name                                      = "${local.resource_name_prefix}inst-geneve"
      protocol                                  = "GENEVE"
      type                                      = "instance"
      gateway_target_failover_enabled           = true
      gateway_target_failover_on_deregistration = "rebalance"
      gateway_target_failover_on_unhealthy      = "rebalance"
    }
  }

  listeners = {
    geneve = {
      target_group_key = "instance"
    }
  }

  vpc_id = data.aws_vpc.default.id
}

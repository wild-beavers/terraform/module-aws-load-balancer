####
# Variable
####

variable "name" {
  type        = string
  description = "The name of the load balancer."
  default     = null

  validation {
    condition     = var.name == null || (can(regex("^[^\\-][\\w\\-]{1,31}[\\w]$", var.name)) && !startswith(coalesce(var.name, "undefined"), "internal-"))
    error_message = "“var.name” must match “^[^\\-][\\w\\-]{1,31}[\\w]$” and cannot start with “internal-”."
  }
}

variable "is_name_prefix" {
  type        = bool
  description = "Creates a unique name beginning with the specified prefix in “var.name”."
  default     = false
}

variable "access_logs_s3_bucket_name" {
  type        = string
  description = "The name of the S3 bucket name to store the logs in. Only valid if “var.access_logs_enabled” is set."
  default     = null
}

variable "access_logs_prefix" {
  type        = string
  description = "The S3 bucket prefix. Logs are stored in the root if not configured. Only valid if “var.access_logs_enabled” and “var.access_logs_s3_bucket_name” are set."
  default     = null
}

variable "customer_owned_ipv4_pool" {
  type        = string
  description = "The ID of the customer-owned ipv4 pool to use for this load balancer."
  default     = null

  validation {
    condition     = var.customer_owned_ipv4_pool == null || can(regex("^ipv4pool-coip-[a-zA-Z0-9]{1,256}$", var.customer_owned_ipv4_pool))
    error_message = "“var.customer_owned_ipv4_pool” must match “^ipv4pool-coip-[a-zA-Z0-9]{1,256}$”."
  }
}

variable "desync_mitigation_mode" {
  type        = string
  description = "Determines how the load balancer handles requests that might pose a security risk to an application due to HTTP desync. Valid values are “monitor”, “defensive”, “strictest”."
  default     = null

  validation {
    condition     = var.desync_mitigation_mode == null || try(contains(["monitor", "defensive", "strictest"], var.desync_mitigation_mode), false)
    error_message = "“var.desync_mitigation_mode” must be one of “monitor”, “defensive” or “strictest”."
  }
}

variable "application_drop_invalid_header_fields" {
  type        = bool
  description = "Indicates whether HTTP headers with header fields that are not valid are removed by the load balancer (true) or routed to targets (false). Elastic Load Balancing requires that message header names contain only alphanumeric characters and hyphens."
  default     = true
  nullable    = false
}

variable "cross_zone_load_balancing_enabled" {
  type        = bool
  description = "Whether to enable cross-zone load balancing of the load balancer or not."
  default     = false
}

variable "deletion_protection_enabled" {
  type        = bool
  description = "Whether to enable deletion protection of the load balancer via the AWS API or not."
  default     = false
}

variable "application_http2_enabled" {
  type        = bool
  description = "Whether to enable HTTP/2 in application load balancer or not."
  default     = true
}

variable "application_tls_version_and_cipher_suite_headers_enabled" {
  type        = bool
  description = "Whether to add the two headers (“x-amzn-tls-version” and “x-amzn-tls-cipher-suite”), which contain information about the negotiated TLS version and cipher suite or not."
  default     = false
}

variable "application_xff_client_port_enabled" {
  type        = bool
  description = "Whether to preserve the “X-Forwarded-For” header with the source port that the client used to connect to the load balancer or not."
  default     = false
}

variable "application_enable_xff_client_port" {
  type        = bool
  description = "DEPRECATED. Use “var.application_xff_client_port_enabled” instead."
  default     = false
}

variable "application_waf_fail_open_enabled" {
  type        = bool
  description = "Whether to allow a WAF-enabled load balancer to route requests to targets if it is unable to forward the request to AWS WAF or not."
  default     = false
}

variable "application_idle_timeout" {
  type        = number
  description = "The time in seconds that the connection is allowed to be idle."
  default     = null

  validation {
    condition     = var.application_idle_timeout == null || coalesce(var.application_idle_timeout, 1) > 0
    error_message = "“var.application_idle_timeout” must be a strictly positive number."
  }
}

variable "internal" {
  type        = bool
  description = "Whether the load balancer is internal or not."
  default     = false
}

variable "ip_address_type" {
  type        = string
  description = "The type of IP addresses used by the subnets for your load balancer. The possible values are “ipv4” and “dualstack”."
  default     = null

  validation {
    condition     = var.ip_address_type == null || try(contains(["ipv4", "dualstack"], var.ip_address_type), false)
    error_message = "“var.ip_address_type” value must be one of “ipv4” or “ip_address_type”"
  }
}
variable "type" {
  type        = string
  description = "The type of load balancer to create. Possible values are “application”, “gateway”, or “network”."
  default     = null

  validation {
    condition     = var.type == null || try(contains(["application", "gateway", "network"], var.type), false)
    error_message = "“var.type” must be one of “application”, “gateway” or “network”."
  }
}

variable "security_groups" {
  type        = list(string)
  description = "A list of security group IDs to assign to the load balancer"
  default     = null

  validation {
    condition = length(coalesce(var.security_groups, [])) == 0 || !contains(
      [for security_group_id in coalesce(var.security_groups, []) : can(regex("^sg-([a-z0-9]{8}|[a-z0-9]{17})$", security_group_id))]
    , false)
    error_message = "One or more of the “var.application_security_groups” does not match “^sg-([a-z0-9]{8}|[a-z0-9]{17})$”."
  }
}

variable "application_preserve_host_header" {
  type        = bool
  description = "Whether to preserve the Host header in the HTTP request and send it to the target without any change or not."
  default     = false
}

variable "application_xff_header_processing_mode" {
  type        = string
  description = "Determines how the load balancer will modify the X-Forwarded-For header in the HTTP request before sending the request to the target. The possible values are “append”, “preserve”, and “remove”."
  default     = null

  validation {
    condition     = var.application_xff_header_processing_mode == null || try(contains(["append", "preserve", "remove"], var.application_xff_header_processing_mode), false)
    error_message = "“var.application_xff_header_processing_mode” must be one of “append”, “preserve” or “remove”."
  }
}

variable "subnets" {
  type = map(object({
    subnet_id     = string
    allocation_id = optional(string)
    ipv6_address  = optional(string)
    ipv4_address  = optional(string)
  }))
  description = <<DOCUMENTATION
Map of subnets where to create the load balancer.
Keys are free value. Values are:
- subnet_id     (string, required): The ID of the subnet. You can specify only one subnet per Availability Zone.
- allocation_id (string, optional): The allocation ID of the Elastic IP address for an internet-facing load balancer.
- ipv6_address  (string, optional): The IPv6 address. You associate IPv6 CIDR blocks with your VPC and choose the subnets where you launch both internet-facing and internal Application Load Balancers or Network Load Balancers.
- ipv4_address  (string, optional): The private IPv4 address for an internal load balancer.
DOCUMENTATION
  default     = null

  validation {
    condition = var.subnets == null || !contains(
      [for k, v in coalesce(var.subnets, {}) : (
        can(regex("^subnet-([a-z0-9]{8}|[a-z0-9]{17})$", v.subnet_id)) &&
        (v.allocation_id == null || can(regex("^eipalloc-([a-z0-9]{8}|[a-z0-9]{17}$", v.allocation_id))) &&
        (v.ipv6_address == null || can(cidrhost(format("%s/31", v.ipv6_address), 1))) &&
        (v.ipv4_address == null || can(cidrhost(format("%s/31", v.ipv4_address), 1)))
        )
    ], false)
    error_message = "One of more elements of “var.subnets” are not valid. Check the variable definition for more details."
  }
}

variable "load_balancer_tags" {
  type        = map(string)
  description = "Tags to be applied to the load balancer, merge with var.tags."
  default     = {}
}

variable "vpc_id" {
  type        = string
  description = "The ID of the VPC where to deploy resources."
  default     = null

  validation {
    condition     = var.vpc_id == null || can(regex("^vpc-([a-z0-9]{8}|[a-z0-9]{17})$", var.vpc_id))
    error_message = "“var.vpc_id” does not match “^vpc-([a-z0-9]{8}|[a-z0-9]{17})$”."
  }
}

variable "tags" {
  type        = map(string)
  description = "Tags to be applied on all resources of this module."
  default     = {}
}

####
# Locals
####

locals {
  tags = merge(
    {
      managed-by = "terraform"
    },
    var.tags,
    {
      origin = "gitlab.com/wild-beavers/terraform/module-aws-load-balancer"
    }
  )
  is_application_load_balancer = var.type == "application" || var.type == null
  is_gateway_load_balancer     = var.type == "gateway"
}

####
# Resources & data
####

resource "aws_lb" "this" {
  for_each = { 0 = 0 }

  name        = var.is_name_prefix == false ? var.name : null
  name_prefix = var.is_name_prefix == true ? var.name : null

  dynamic "access_logs" {
    for_each = coalesce(var.access_logs_s3_bucket_name, "undefined") != "undefined" ? { 0 = 0 } : {}

    content {
      enabled = true
      bucket  = var.access_logs_s3_bucket_name
      prefix  = var.access_logs_prefix
    }
  }
  customer_owned_ipv4_pool                    = var.customer_owned_ipv4_pool
  desync_mitigation_mode                      = var.desync_mitigation_mode
  drop_invalid_header_fields                  = var.application_drop_invalid_header_fields
  enable_cross_zone_load_balancing            = var.cross_zone_load_balancing_enabled
  enable_deletion_protection                  = var.deletion_protection_enabled
  enable_http2                                = var.application_http2_enabled
  enable_tls_version_and_cipher_suite_headers = var.application_tls_version_and_cipher_suite_headers_enabled
  enable_xff_client_port                      = var.application_xff_client_port_enabled || var.application_enable_xff_client_port
  enable_waf_fail_open                        = var.application_waf_fail_open_enabled
  idle_timeout                                = var.application_idle_timeout
  internal                                    = var.internal
  ip_address_type                             = var.ip_address_type
  load_balancer_type                          = var.type
  security_groups                             = var.security_groups
  preserve_host_header                        = var.application_preserve_host_header
  xff_header_processing_mode                  = var.application_xff_header_processing_mode

  dynamic "subnet_mapping" {
    for_each = coalesce(var.subnets, {})

    content {
      subnet_id            = subnet_mapping.value.subnet_id
      allocation_id        = subnet_mapping.value.allocation_id
      ipv6_address         = subnet_mapping.value.ipv6_address
      private_ipv4_address = subnet_mapping.value.ipv4_address
    }
  }

  tags = merge(
    local.tags,
    var.load_balancer_tags,
    {
      Name = var.name
    }
  )
}

variable "listeners" {
  type = map(object({
    target_group_key = optional(string)
    target_group_arn = optional(string)

    alpn_policy     = optional(string)
    certificate_arn = optional(string)
    ssl_policy      = optional(string)
    port            = optional(number)
    protocol        = optional(string)

    tags = optional(map(string))
  }))
  description = <<DOCUMENTATION
Map of listeners. This applies only for “network” and “gateway” load balancer types. For “application” load balancer type, use “var.alb_listeners”
Keys are free values.
- target_group_key (optional, string):      Key of the “var.target_groups” where the listener must be attached to. Conflicts with “target_group_arn”. One of “target_group_arn” or “target_group_key” is mandatory.
- target_group_arn (optional, string):      Arn of the target group where the listener must be attached to. Conflicts with “target_group_key”. One of “target_group_arn” or “target_group_key” is mandatory.
- alpn_policy      (optional, string):      Name of the Application-Layer Protocol Negotiation (ALPN) policy. Can be set if protocol is “TLS”. Valid values are “HTTP1Only”, “HTTP1Only”, “HTTP2Optional”, “HTTP2Preferred”, and “None”.
- certificate_arn  (optional, string):      ARN of the default SSL server certificate. Can be set if protocol is “TLS”.
- ssl_policy       (optional, string):      Name of the SSL Policy for the listener. Required if protocol “TLS”.
- port             (optional, number):      Port on which the load balancer is listening. Not valid for “gateway” load balancers.
- protocol         (optional, string):      Protocol for connections from clients to the load balancer. Valid values are “TCP”, “TLS”, “UDP”, and “TCP_UDP”. Not valid for “gateway” load balancers.
- tags             (optional, map(string)): Tags to be applied to the listener, merge with var.tags.
DOCUMENTATION
  default     = {}
  nullable    = false

  validation {
    condition = !contains([for k, v in var.listeners : (
      (v.target_group_arn == null || can(regex("^arn:(?:aws|aws-cn|aws-us-gov):elasticloadbalancing:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:targetgroup\\/[^\\-][\\w\\-]{1,31}\\/[[:xdigit:]]{16}$", v.target_group_arn))) &&
      (v.alpn_policy == null || contains(["HTTP1Only", "HTTP1Only", "HTTP2Optional", "HTTP2Preferred", "None"], coalesce(v.alpn_policy, "undefined"))) &&
      (v.certificate_arn == null || can(regex("^arn:(?:aws|aws-cn|aws-us-gov):acm:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:certificate\\/[a-z0-9-]{36}$", v.certificate_arn))) &&
      (v.ssl_policy == null || can(regex("ELBSecurityPolicy-*", coalesce(v.ssl_policy, "undefined")))) &&
      (v.port == null || (coalesce(v.port, -1) > 0 && (coalesce(v.port, 65536) <= 65535))) &&
      (v.protocol == null || contains(["TCP", "TLS", "UDP", "TCP_UDP"], coalesce(v.protocol, "undefined")))
    )], false)
    error_message = "One or more “var.listeners” are not valid."
  }
}

variable "listeners_tags" {
  type        = map(string)
  description = "Tags to be applied and merge with all listeners."
  default     = {}
}

resource "aws_lb_listener" "non_alb" {
  for_each = var.listeners

  load_balancer_arn = aws_lb.this["0"].arn

  alpn_policy     = each.value.alpn_policy
  certificate_arn = each.value.certificate_arn
  ssl_policy      = each.value.ssl_policy

  port     = each.value.port
  protocol = each.value.protocol

  default_action {
    type             = "forward"
    target_group_arn = each.value.target_group_key != null ? aws_lb_target_group.this[each.value.target_group_key].arn : each.value.target_group_arn
  }

  tags = merge(
    local.tags,
    var.listeners_tags,
    each.value.tags,
  )

  lifecycle {
    precondition {
      condition     = !local.is_application_load_balancer
      error_message = "“var.listeners” is reserved for “network” and “gateway” load balancer type. Please use “var.application_listeners” for an “application” load balancer."
    }

    precondition {
      condition     = !(local.is_gateway_load_balancer && length(var.listeners) > 1)
      error_message = "Listener “${each.key}”: gateway load balancer can only accept 1 listener. Given “${length(var.listeners)}”"
    }

    precondition {
      condition     = !((each.value.target_group_key == null && each.value.target_group_arn == null) || (each.value.target_group_key != null && each.value.target_group_arn != null))
      error_message = "Listener “${each.key}”: you must set exclusively one of “target_group_arn” or “target_group_key” in “var.listeners”"
    }
  }
}

####
# Application Load Balancer listener
####

variable "alb_listeners" {
  type = map(object({
    port        = number
    protocol    = string
    action_type = string

    certificate_arn = optional(string)
    ssl_policy      = optional(string)

    authenticate_cognito_user_pool_arn                       = optional(string)
    authenticate_cognito_user_pool_client_id                 = optional(string)
    authenticate_cognito_user_pool_domain                    = optional(string)
    authenticate_cognito_authentication_request_extra_params = optional(map(object({ key = string, value = string })))
    authenticate_cognito_on_unauthenticated_request          = optional(string)
    authenticate_cognito_scope                               = optional(set(string))
    authenticate_cognito_session_cookie_name                 = optional(string)
    authenticate_cognito_session_timeout                     = optional(number)

    authenticate_oidc_authorization_endpoint              = optional(string)
    authenticate_oidc_client_id                           = optional(string)
    authenticate_oidc_client_secret                       = optional(string)
    authenticate_oidc_issuer                              = optional(string)
    authenticate_oidc_token_endpoint                      = optional(string)
    authenticate_oidc_user_info_endpoint                  = optional(string)
    authenticate_oidc_authentication_request_extra_params = optional(map(object({ key = string, value = string })))
    authenticate_oidc_on_unauthenticated_request          = optional(string)
    authenticate_oidc_scope                               = optional(set(string))
    authenticate_oidc_session_cookie_name                 = optional(string)
    authenticate_oidc_session_timeout                     = optional(number)

    fixed_response_content_type = optional(string)
    fixed_response_message_body = optional(string)
    fixed_response_status_code  = optional(number)

    target_groups = optional(set(object({
      key    = optional(string)
      arn    = optional(string)
      weight = optional(number)
    })))

    stickiness_duration = optional(number)

    redirect_status_code = optional(string)
    redirect_host        = optional(string)
    redirect_path        = optional(string)
    redirect_port        = optional(any)
    redirect_protocol    = optional(string)
    redirect_query       = optional(string)

    tags = optional(map(string))
  }))
  description = <<DOCUMENTATION
Map of listeners. This applies only for “application” load balancer types. For “network” and “gateway” load balancer type, use “var.listeners”
Keys are free values.
- action_type (required, string)     : Type of routing action. Valid values are “forward”, “redirect”, “fixed-response”, “authenticate-cognito” and “authenticate-oidc”.
- port (required, number)            : Port on which the load balancer is listening.
- protocol (required, string)        : Protocol for connections from clients to the load balancer. Valid values are “HTTP” and “HTTPS”.
- certificate_arn (optional, string) : ARN of the default SSL server certificate. Required if protocol “HTTPS”.
- ssl_policy (optional, string)      : Name of the SSL Policy for the listener. Required if protocol “HTTPS”.

- authenticate_cognito_user_pool_arn (optional, string)                                                             : ARN of the Cognito user pool.
- authenticate_cognito_user_pool_client_id (optional, string)                                                       : ID of the Cognito user pool client.
- authenticate_cognito_user_pool_domain (optional, string)                                                          : Domain prefix or fully-qualified domain name of the Cognito user pool.
- authenticate_cognito_authentication_request_extra_params (optional, map(object({ key = string, value = string }))): Query parameters to include in the redirect request to the authorization endpoint. Max: 10.
- authenticate_cognito_on_unauthenticated_request (optional, string)                                                : Behavior if the user is not authenticated. Valid values are “deny”, “allow” and “authenticate”.
- authenticate_cognito_scope (optional, set(string))                                                                : Set of user claims to be requested from the IdP.
- authenticate_cognito_session_cookie_name (optional, string)                                                       : Name of the cookie used to maintain session information.
- authenticate_cognito_session_timeout (optional, number)                                                           : Maximum duration of the authentication session, in seconds.

- authenticate_oidc_authorization_endpoint (optional, string)                                                    : Authorization endpoint of the IdP.
- authenticate_oidc_client_id (optional, string)                                                                 : OAuth 2.0 client identifier.
- authenticate_oidc_client_secret (optional, string)                                                             : OAuth 2.0 client secret.
- authenticate_oidc_issuer (optional, string)                                                                    : OIDC issuer identifier of the IdP.
- authenticate_oidc_token_endpoint (optional, string)                                                            : Token endpoint of the IdP.
- authenticate_oidc_user_info_endpoint (optional, string)                                                        : User info endpoint of the IdP.
- authenticate_oidc_authentication_request_extra_params (optional, map(object({ key = string, value = string }))): Query parameters to include in the redirect request to the authorization endpoint. Max: 10.
- authenticate_oidc_on_unauthenticated_request (optional, string)                                                : Behavior if the user is not authenticated. Valid values: “deny”, “allow” and “authenticate”.
- authenticate_oidc_scope (optional, set(string))                                                                : Set of user claims to be requested from the IdP.
- authenticate_oidc_session_cookie_name (optional, string)                                                       : Name of the cookie used to maintain session information.
- authenticate_oidc_session_timeout (optional, number)                                                           : Maximum duration of the authentication session, in seconds.

- fixed_response_content_type (optional, string): Content type. Valid values are “text/plain”, “text/css”, “text/html”, “application/javascript” and “application/json”.
- fixed_response_message_body (optional, string): Message body.
- fixed_response_status_code (optional, number) : TTP response code. Valid values are 2XX, 4XX, or 5XX.

- target_groups (optional, set(object({key = optional(string), arn = optional(string), weight = optional(number)}))): Set of object of target groups. “key” is the “var.target_groups”, “arn” is the ARN of the target group, “weight” is the range between 0 and 999. One of “arn” or “key” is mandatory, and you must exclusively use one of “arn” or “key”

- stickiness_duration (optional, number): Time period, in seconds, during which requests from a client should be routed to the same target group. The range is 1-604800 seconds (7 days).

- redirect_status_code (optional, string): HTTP redirect code. The redirect is either permanent (“HTTP_301”) or temporary (“HTTP_302”).
- redirect_host (optional, string): Hostname. This component is not percent-encoded. The hostname can contain #{host}.
- redirect_path (optional, string): Absolute path, starting with the leading “/”. This component is not percent-encoded. The path can contain “#{host}”, “#{path}”, and “#{port}”.
- redirect_port (optional, any): Port. Specify a value from 1 to 65535 or “#{port}“.
- redirect_protocol (optional, string): Protocol. Valid values are “HTTP”, “HTTPS”, or “#{protocol}”.
- redirect_query (optional, string): Query parameters, URL-encoded when necessary, but not percent-encoded. Do not include the leading “?”.

- tags (optional, map(string))        : Tags to be applied and merge with this listener.
DOCUMENTATION
  default     = {}
  nullable    = false

  validation {
    condition = !contains([for k, v in var.alb_listeners : (
      (v.certificate_arn == null || can(regex("^arn:(?:aws|aws-cn|aws-us-gov):acm:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:certificate\\/[a-z0-9-]{36}$", v.certificate_arn))) &&
      (v.ssl_policy == null || can(regex("ELBSecurityPolicy-*", coalesce(v.ssl_policy, "undefined")))) &&
      (v.port == null || (coalesce(v.port, -1) > 0 && (coalesce(v.port, 65536) <= 65535))) &&
      (v.protocol == null || contains(["HTTP", "HTTPS"], coalesce(v.protocol, "undefined"))) &&
      contains(["forward", "redirect", "fixed-response", "authenticate-cognito", "authenticate-oidc"], coalesce(v.action_type, "undefined")) &&
      (v.authenticate_cognito_user_pool_arn == null || can(regex("^arn:(?:aws|aws-cn|aws-us-gov):cognito-idp:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:userpool\\/[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}_\\d{9}$", v.authenticate_cognito_user_pool_arn))) &&
      (v.authenticate_cognito_on_unauthenticated_request == null || contains(["allow", "deny", "authenticate"], coalesce(v.authenticate_cognito_on_unauthenticated_request, "undefined"))) &&
      (v.authenticate_cognito_session_timeout == null || (coalesce(v.authenticate_cognito_session_timeout, -1) > 0 && coalesce(v.authenticate_cognito_session_timeout, 604801) <= 604800)) &&
      (v.authenticate_oidc_on_unauthenticated_request == null || contains(["allow", "deny", "authenticate"], coalesce(v.authenticate_oidc_on_unauthenticated_request, "undefined"))) &&
      (v.authenticate_oidc_session_timeout == null || contains(["allow", "deny", "authenticate"], coalesce(v.authenticate_oidc_session_timeout, "undefined"))) &&
      (v.fixed_response_content_type == null || contains(["text/plain", "text/css", "text/html", "application/javascript", "application/json"], coalesce(v.fixed_response_content_type, "undefined"))) &&
      (v.fixed_response_status_code == null || (
        (coalesce(v.fixed_response_status_code, 0) >= 200 && coalesce(v.fixed_response_status_code, 300) < 300) ||
        (coalesce(v.fixed_response_status_code, 0) >= 400 && coalesce(v.fixed_response_status_code, 600) < 600)
      )) &&
      (!contains([for target in coalesce(v.target_groups, toset([])) : (
        target.weight == null || (coalesce(target.weight, -1) > 0 && coalesce(target.weight, 1000) < 1000)
      )], false)) &&
      (v.target_groups == null || (length(coalesce(v.target_groups, toset([]))) > 0 && length(coalesce(v.target_groups, toset([]))) < 6)) &&
      (alltrue([for target_group in coalesce(v.target_groups, toset([])) : (target_group.arn == null || can(regex("^arn:(?:aws|aws-cn|aws-us-gov):elasticloadbalancing:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:targetgroup\\/[^\\-][\\w\\-]{1,31}\\/[[:xdigit:]]{16}$", target_group.arn)))])) &&
      (v.stickiness_duration == null || (coalesce(v.stickiness_duration, -1) >= 0 && coalesce(v.stickiness_duration, 604801) < 604800)) &&
      (v.redirect_status_code == null || contains(["HTTP_301", "HTTP_302"], coalesce(v.redirect_status_code, "undefined"))) &&
      (v.redirect_path == null || can(regex("^\\/.*$", v.redirect_path))) &&
      (v.redirect_port == null || v.redirect_port == "#{port}" || (coalesce(v.redirect_port, 0) > 0 && coalesce(v.redirect_port, 65536) < 65536)) &&
      (v.redirect_protocol == null || contains(["HTTP", "HTTPS", "#{protocol}"], coalesce(v.redirect_protocol, "undefined"))) &&
      (v.redirect_query == null || can(regex("^(?:[^\\?]).+$", v.redirect_query)))
    )], false)
    error_message = "One or more “var.alb_listeners” are not valid."
  }
}

variable "alb_listeners_tags" {
  type        = map(string)
  description = "Tags to be applied to the ALB listeners resources, merged with var.tags."
  default     = {}
}

resource "aws_lb_listener" "alb" {
  for_each = var.alb_listeners

  load_balancer_arn = aws_lb.this["0"].arn

  certificate_arn = each.value.certificate_arn
  ssl_policy      = each.value.ssl_policy

  port     = each.value.port
  protocol = each.value.protocol

  tags = merge(
    local.tags,
    var.alb_listeners_tags,
    each.value.tags,
  )

  default_action {
    type = each.value.action_type

    dynamic "authenticate_cognito" {
      for_each = each.value.action_type == "authenticate-cognito" ? { 0 = 0 } : {}

      content {
        user_pool_arn       = each.value.authenticate_cognito_user_pool_arn
        user_pool_client_id = each.value.authenticate_cognito_user_pool_client_id
        user_pool_domain    = each.value.authenticate_cognito_user_pool_domain

        authentication_request_extra_params = each.value.authenticate_cognito_authentication_request_extra_params
        on_unauthenticated_request          = each.value.authenticate_cognito_on_unauthenticated_request
        scope                               = each.value.authenticate_cognito_scope
        session_cookie_name                 = each.value.authenticate_cognito_session_cookie_name
        session_timeout                     = each.value.authenticate_cognito_session_timeout
      }
    }

    dynamic "authenticate_oidc" {
      for_each = each.value.action_type == "authenticate-oidc" ? { 0 = 0 } : {}

      content {
        authorization_endpoint = each.value.authenticate_oidc_authorization_endpoint
        client_id              = each.value.authenticate_oidc_client_id
        client_secret          = sensitive(each.value.authenticate_oidc_client_secret)
        issuer                 = each.value.authenticate_oidc_issuer
        token_endpoint         = each.value.authenticate_oidc_token_endpoint
        user_info_endpoint     = each.value.authenticate_oidc_user_info_endpoint

        authentication_request_extra_params = each.value.authenticate_oidc_authentication_request_extra_params
        on_unauthenticated_request          = each.value.authenticate_oidc_on_unauthenticated_request
        scope                               = each.value.authenticate_oidc_scope
        session_cookie_name                 = each.value.authenticate_oidc_session_cookie_name
        session_timeout                     = each.value.authenticate_oidc_session_timeout
      }
    }

    dynamic "fixed_response" {
      for_each = each.value.action_type == "fixed-response" ? { 0 = 0 } : {}

      content {
        content_type = each.value.fixed_response_content_type
        message_body = each.value.fixed_response_message_body
        status_code  = each.value.fixed_response_status_code
      }
    }

    # Because of AWS provider idempotency issue, we can't use the forward block to setup a single target group
    target_group_arn = length(coalesce(each.value.target_groups, toset([]))) == 1 ? tolist(each.value.target_groups)[0].key != null ? aws_lb_target_group.this[tolist(each.value.target_groups)[0].key].arn : tolist(each.value.target_groups)[0].arn : null

    dynamic "forward" {
      for_each = each.value.action_type == "forward" && length(coalesce(each.value.target_groups, toset([]))) > 1 ? { 0 = 0 } : {}

      content {
        dynamic "target_group" {
          for_each = coalesce(each.value.target_groups, toset([]))
          content {
            arn    = target_group.value.key != null ? aws_lb_target_group.this[target_group.value.key].arn : target_group.value.arn
            weight = target_group.value.weight
          }
        }

        dynamic "stickiness" {
          for_each = coalesce(each.value.stickiness_duration, -1) != -1 ? { 0 = 0 } : {}

          content {
            duration = each.value.stickiness_duration
            enabled  = true
          }
        }
      }
    }

    dynamic "redirect" {
      for_each = each.value.action_type == "redirect" ? { 0 = 0 } : {}

      content {
        status_code = each.value.redirect_status_code

        host     = each.value.redirect_host
        path     = each.value.redirect_path
        port     = each.value.redirect_port
        protocol = each.value.redirect_protocol
        query    = each.value.redirect_query
      }
    }
  }

  lifecycle {
    precondition {
      condition     = !contains([for target_group in coalesce(each.value.target_groups, toset([])) : ((target_group.key == null && target_group.arn == null) || (target_group.key != null && target_group.arn != null))], true)
      error_message = "Listener “${each.key}”: you must set exclusively one of “arn” or “key” in target_groups objects in “var.alb_listeners”"
    }
  }
}

####
# ALB listener rules
####

variable "alb_listener_rules" {
  type = map(object({
    listener_key = string
    action_type  = string

    priority = optional(number)

    authenticate_cognito_user_pool_arn                       = optional(string)
    authenticate_cognito_user_pool_client_id                 = optional(string)
    authenticate_cognito_user_pool_domain                    = optional(string)
    authenticate_cognito_authentication_request_extra_params = optional(map(object({ key = string, value = string })))
    authenticate_cognito_on_unauthenticated_request          = optional(string)
    authenticate_cognito_scope                               = optional(set(string))
    authenticate_cognito_session_cookie_name                 = optional(string)
    authenticate_cognito_session_timeout                     = optional(number)

    authenticate_oidc_authorization_endpoint              = optional(string)
    authenticate_oidc_client_id                           = optional(string)
    authenticate_oidc_client_secret                       = optional(string)
    authenticate_oidc_issuer                              = optional(string)
    authenticate_oidc_token_endpoint                      = optional(string)
    authenticate_oidc_user_info_endpoint                  = optional(string)
    authenticate_oidc_authentication_request_extra_params = optional(map(object({ key = string, value = string })))
    authenticate_oidc_on_unauthenticated_request          = optional(string)
    authenticate_oidc_scope                               = optional(set(string))
    authenticate_oidc_session_cookie_name                 = optional(string)
    authenticate_oidc_session_timeout                     = optional(number)

    fixed_response_content_type = optional(string)
    fixed_response_message_body = optional(string)
    fixed_response_status_code  = optional(number)

    target_groups = optional(set(object({
      key    = optional(string)
      arn    = optional(string)
      weight = optional(number)
    })))

    stickiness_duration = optional(number)

    redirect_status_code = optional(string)
    redirect_host        = optional(string)
    redirect_path        = optional(string)
    redirect_port        = optional(any)
    redirect_protocol    = optional(string)
    redirect_query       = optional(string)

    host_headers = optional(list(string))
    http_headers = optional(set(object({
      name   = string
      values = list(string)
    })))

    http_request_methods = optional(list(string))

    path_patterns = optional(list(string))

    query_strings = optional(set(object({
      key   = string
      value = string
    })))

    source_ips = optional(list(string))

    tags = optional(map(string))
  }))
  description = <<DOCUMENTATION
Map of listener. This apply only for “application” load balancer type.
Keys are free values.

- listener_key (required, string)    : Key of the “var.alb_listeners” where to add this rule.
- action_type (required, string)     : Type of routing action. Valid values are “forward”, “redirect”, “fixed-response”, “authenticate-cognito” and “authenticate-oidc”.
- priority (optional, number)        : The priority for the rule between “1” and “50000”. Leaving it unset will automatically set the rule with next available priority after currently existing highest rule. A listener can't have multiple rules with the same priority.

- authenticate_cognito_user_pool_arn (optional, string)                                                             : ARN of the Cognito user pool.
- authenticate_cognito_user_pool_client_id (optional, string)                                                       : ID of the Cognito user pool client.
- authenticate_cognito_user_pool_domain (optional, string)                                                          : Domain prefix or fully-qualified domain name of the Cognito user pool.
- authenticate_cognito_authentication_request_extra_params (optional, map(object({ key = string, value = string }))): Query parameters to include in the redirect request to the authorization endpoint. Max: 10.
- authenticate_cognito_on_unauthenticated_request (optional, string)                                                : Behavior if the user is not authenticated. Valid values are “deny”, “allow” and “authenticate”.
- authenticate_cognito_scope (optional, set(string))                                                                : Set of user claims to be requested from the IdP.
- authenticate_cognito_session_cookie_name (optional, string)                                                       : Name of the cookie used to maintain session information.
- authenticate_cognito_session_timeout (optional, number)                                                           : Maximum duration of the authentication session, in seconds.

- authenticate_oidc_authorization_endpoint (optional, string)                                                    : Authorization endpoint of the IdP.
- authenticate_oidc_client_id (optional, string)                                                                 : OAuth 2.0 client identifier.
- authenticate_oidc_client_secret (optional, string)                                                             : OAuth 2.0 client secret.
- authenticate_oidc_issuer (optional, string)                                                                    : OIDC issuer identifier of the IdP.
- authenticate_oidc_token_endpoint (optional, string)                                                            : Token endpoint of the IdP.
- authenticate_oidc_user_info_endpoint (optional, string)                                                        : User info endpoint of the IdP.
- authenticate_oidc_authentication_request_extra_params (optional, map(object({ key = string, value = string }))): Query parameters to include in the redirect request to the authorization endpoint. Max: 10.
- authenticate_oidc_on_unauthenticated_request (optional, string)                                                : Behavior if the user is not authenticated. Valid values: “deny”, “allow” and “authenticate”.
- authenticate_oidc_scope (optional, set(string))                                                                : Set of user claims to be requested from the IdP.
- authenticate_oidc_session_cookie_name (optional, string)                                                       : Name of the cookie used to maintain session information.
- authenticate_oidc_session_timeout (optional, number)                                                           : Maximum duration of the authentication session, in seconds.

- fixed_response_content_type (optional, string): Content type. Valid values are “text/plain”, “text/css”, “text/html”, “application/javascript” and “application/json”.
- fixed_response_message_body (optional, string): Message body.
- fixed_response_status_code (optional, number) : TTP response code. Valid values are 2XX, 4XX, or 5XX.

- target_groups (optional, set(object({key = optional(string), arn = optional(string), weight = optional(number)}))): Set of object of target groups. “key” is the “var.target_groups”, “arn” is the ARN of the target group, “weight” is the range between 0 and 999. One of “arn” or “key” is mandatory, and you must exclusively use one of “arn” or “key”

- stickiness_duration (optional, number): Time period, in seconds, during which requests from a client should be routed to the same target group. The range is 1-604800 seconds (7 days).

- redirect_status_code (optional, string): HTTP redirect code. The redirect is either permanent (“HTTP_301”) or temporary (“HTTP_302”).
- redirect_host (optional, string)       : Hostname. This component is not percent-encoded. The hostname can contain #{host}.
- redirect_path (optional, string)       : Absolute path, starting with the leading “/”. This component is not percent-encoded. The path can contain “#{host}”, “#{path}”, and “#{port}”.
- redirect_port (optional, any)          : Port. Specify a value from 1 to 65535 or “#{port}“.
- redirect_protocol (optional, string)   : Protocol. Valid values are “HTTP”, “HTTPS”, or “#{protocol}”.
- redirect_query (optional, string)      : Query parameters, URL-encoded when necessary, but not percent-encoded. Do not include the leading “?”.

- host_headers (optional, list(string)): List of host header patterns to match. The maximum size of each pattern is 128 characters. Comparison is case insensitive. Wildcard characters supported: \\* (matches 0 or more characters) and ? (matches exactly 1 character). Only one pattern needs to match for the condition to be satisfied.

- http_headers (optional, set(object({name = string, values = list(string)}))): Keys are free values.
  Values are:
    - name (required, string)        : Name of HTTP header to search. The maximum size is 40 characters. Comparison is case insensitive. Only RFC7240 characters are supported. Wildcards are not supported.
    - values (required, list(string)): List of header value patterns to match. Maximum size of each pattern is 128 characters. Comparison is case insensitive. Wildcard characters supported: \\* (matches 0 or more characters) and ? (matches exactly 1 character). If the same header appears multiple times in the request they will be searched in order until a match is found. Only one pattern needs to match for the condition to be satisfied.

- http_request_methods (optional, list(string)): List of HTTP request methods or verbs to match. Maximum size is 40 characters. Only allowed characters are A-Z, hyphen (-) and underscore (_). Comparison is case sensitive. Wildcards are not supported. Only one needs to match for the condition to be satisfied. AWS recommends that GET and HEAD requests are routed in the same way because the response to a HEAD request may be cached.

- path_patterns (optional, list(string)): Llist of path patterns to match against the request URL. Maximum size of each pattern is 128 characters. Comparison is case sensitive. Wildcard characters supported: \\* (matches 0 or more characters) and ? (matches exactly 1 character). Only one pattern needs to match for the condition to be satisfied. Path pattern is compared only to the path of the URL, not to its query string.

- query_strings (optional, set(object({key = string, value = string}))): Query string pairs or values to match. Keys are free values.
  Values are:
   - key (required, string)  : Query string key pattern to match.
   - value (required, string): Query string value pattern to match.

- source_ips (optional, list(string)): List of source IP CIDR notations to match. You can use both IPv4 and IPv6 addresses. Wildcards are not supported. Condition is satisfied if the source IP address of the request matches one of the CIDR blocks. Condition is not satisfied by the addresses in the “X-Forwarded-For” header, use “http_headers” condition instead.

- tags (optional, map(string))        : Tags to be applied and merge with this listener.
DOCUMENTATION
  default     = {}
  nullable    = false

  validation {
    condition = !contains([for k, v in var.alb_listener_rules : (
      (v.priority == null || (coalesce(v.priority, 0) > 0 && coalesce(v.priority, 50001) < 50001)) &&
      contains(["forward", "redirect", "fixed-response", "authenticate-cognito", "authenticate-oidc"], coalesce(v.action_type, "undefined")) &&
      (v.authenticate_cognito_user_pool_arn == null || can(regex("^arn:(?:aws|aws-cn|aws-us-gov):cognito-idp:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:userpool\\/[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}_\\d{9}$", v.authenticate_cognito_user_pool_arn))) &&
      (v.authenticate_cognito_on_unauthenticated_request == null || contains(["allow", "deny", "authenticate"], coalesce(v.authenticate_cognito_on_unauthenticated_request, "undefined"))) &&
      (v.authenticate_cognito_session_timeout == null || (coalesce(v.authenticate_cognito_session_timeout, -1) > 0 && coalesce(v.authenticate_cognito_session_timeout, 604801) <= 604800)) &&
      (v.authenticate_oidc_on_unauthenticated_request == null || contains(["allow", "deny", "authenticate"], coalesce(v.authenticate_oidc_on_unauthenticated_request, "undefined"))) &&
      (v.authenticate_oidc_session_timeout == null || contains(["allow", "deny", "authenticate"], coalesce(v.authenticate_oidc_session_timeout, "undefined"))) &&
      (v.fixed_response_content_type == null || contains(["text/plain", "text/css", "text/html", "application/javascript", "application/json"], coalesce(v.fixed_response_content_type, "undefined"))) &&
      (v.fixed_response_status_code == null || (
        (coalesce(v.fixed_response_status_code, 0) >= 200 && coalesce(v.fixed_response_status_code, 300) < 300) ||
        (coalesce(v.fixed_response_status_code, 0) >= 400 && coalesce(v.fixed_response_status_code, 600) < 600)
      )) &&
      (!contains([for target in coalesce(v.target_groups, toset([])) : (
        target.weight == null || (coalesce(target.weight, -1) > 0 && coalesce(target.weight, 1000) < 1000)
      )], false)) &&
      (v.target_groups == null || (length(coalesce(v.target_groups, toset([]))) > 0 && length(coalesce(v.target_groups, toset([]))) < 6)) &&
      (alltrue([for target_group in coalesce(v.target_groups, toset([])) : (target_group.arn == null || can(regex("^arn:(?:aws|aws-cn|aws-us-gov):elasticloadbalancing:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-\\d{1}:\\d{12}:targetgroup\\/[^\\-][\\w\\-]{1,31}\\/[[:xdigit:]]{16}$", target_group.arn)))])) &&
      (v.stickiness_duration == null || (coalesce(v.stickiness_duration, -1) >= 0 && coalesce(v.stickiness_duration, 604801) < 604800)) &&
      (v.redirect_status_code == null || contains(["HTTP_301", "HTTP_302"], coalesce(v.redirect_status_code, "undefined"))) &&
      (v.redirect_path == null || can(regex("^\\/.*$", v.redirect_path))) &&
      (v.redirect_port == null || v.redirect_port == "#{port}" || (coalesce(v.redirect_port, 0) > 0 && coalesce(v.redirect_port, 65536) < 65536)) &&
      (v.redirect_protocol == null || contains(["HTTP", "HTTPS", "#{protocol}"], coalesce(v.redirect_protocol, "undefined"))) &&
      (v.redirect_query == null || can(regex("^(?:[^\\?]).+$", v.redirect_query))) &&
      (v.host_headers == null || !contains([for host_header in coalesce(v.host_headers, []) : can(regex("^[a-zA-Z0-9\\-_\\.;\\*\\?]{1,128}$", host_header))], false)) &&
      (v.http_headers == null || !contains([for key, http_headers in coalesce(v.http_headers, []) : (
        can(regex("^[a-zA-Z0-9\\*\\?\\-!#$%&'+.^_\\x60|~]{1,40}$", key)) &&
        !contains([for http_header in http_headers : can(regex("^[a-zA-Z0-9\\*\\?\\!\"#$%&'()\\+,\\./:;<=>@\\[\\]\\^_\\x60{|}~\\-;]{1,128}$", http_header))], false)
      )], false)) &&
      (v.query_strings == null || !contains([for key, query_strings in coalesce(v.query_strings, []) : (
        can(regex("^[a-zA-Z0-9_\\-.$/~\"'@:+&()\\!,;=\\*\\?]{1,128}$", key)) &&
        !contains([for query_string in query_strings : can(regex("^[a-zA-Z0-9_\\-.$/~\"'@:+&()\\!,;=\\*\\?]{1,128}$", query_string))], false)
      )], false)) &&
      (v.http_request_methods == null || !contains([for http_request_method in coalesce(v.http_request_methods, []) : can(regex("^[A-Z\\-_]{1,40}$", http_request_method))], false)) &&
      (v.path_patterns == null || !contains([for path_pattern in coalesce(v.path_patterns, []) : can(regex("^[a-zA-Z0-9\\*\\?_\\-.$/~\"'@:+; ]{1,128}$", path_pattern))], false)) &&
      (v.source_ips == null || !contains([for source_ip in coalesce(v.source_ips, []) : can(cidrhost(format("%s/31", source_ip), 1))], false))
    )], false)
    error_message = "One or more “var.alb_listener_rules” are not valid."
  }
}

resource "aws_lb_listener_rule" "alb" {
  for_each = var.alb_listener_rules

  listener_arn = aws_lb_listener.alb[each.value.listener_key].arn
  priority     = each.value.priority

  tags = merge(
    local.tags,
    var.alb_listeners_tags,
    each.value.tags,
  )

  condition {
    dynamic "host_header" {
      for_each = length(coalesce(each.value.host_headers, [])) > 0 ? { 0 = 0 } : {}

      content {
        values = each.value.host_headers
      }
    }

    dynamic "http_header" {
      for_each = length(coalesce(each.value.http_headers, toset([]))) > 0 ? { 0 = 0 } : {}

      content {
        http_header_name = http_header.name
        values           = http_header.values
      }
    }

    dynamic "http_request_method" {
      for_each = length(coalesce(each.value.http_request_methods, [])) > 0 ? { 0 = 0 } : {}

      content {
        values = each.value.http_request_methods
      }
    }

    dynamic "path_pattern" {
      for_each = length(coalesce(each.value.path_patterns, [])) > 0 ? { 0 = 0 } : {}

      content {
        values = each.value.path_patterns
      }
    }

    dynamic "query_string" {
      for_each = length(coalesce(each.value.query_strings, toset([]))) > 0 ? { 0 = 0 } : {}

      content {
        key   = query_string.key
        value = query_string.value
      }
    }

    dynamic "source_ip" {
      for_each = length(coalesce(each.value.source_ips, [])) > 0 ? { 0 = 0 } : {}

      content {
        values = each.value.source_ips
      }
    }
  }

  action {
    type = each.value.action_type

    dynamic "authenticate_cognito" {
      for_each = each.value.action_type == "authenticate-cognito" ? { 0 = 0 } : {}

      content {
        user_pool_arn       = each.value.authenticate_cognito_user_pool_arn
        user_pool_client_id = each.value.authenticate_cognito_user_pool_client_id
        user_pool_domain    = each.value.authenticate_cognito_user_pool_domain

        authentication_request_extra_params = each.value.authenticate_cognito_authentication_request_extra_params
        on_unauthenticated_request          = each.value.authenticate_cognito_on_unauthenticated_request
        scope                               = each.value.authenticate_cognito_scope
        session_cookie_name                 = each.value.authenticate_cognito_session_cookie_name
        session_timeout                     = each.value.authenticate_cognito_session_timeout
      }
    }

    dynamic "authenticate_oidc" {
      for_each = each.value.action_type == "authenticate-oidc" ? { 0 = 0 } : {}

      content {
        authorization_endpoint = each.value.authenticate_oidc_authorization_endpoint
        client_id              = each.value.authenticate_oidc_client_id
        client_secret          = sensitive(each.value.authenticate_oidc_client_secret)
        issuer                 = each.value.authenticate_oidc_issuer
        token_endpoint         = each.value.authenticate_oidc_token_endpoint
        user_info_endpoint     = each.value.authenticate_oidc_user_info_endpoint

        authentication_request_extra_params = each.value.authenticate_oidc_authentication_request_extra_params
        on_unauthenticated_request          = each.value.authenticate_oidc_on_unauthenticated_request
        scope                               = each.value.authenticate_oidc_scope
        session_cookie_name                 = each.value.authenticate_oidc_session_cookie_name
        session_timeout                     = each.value.authenticate_oidc_session_timeout
      }
    }

    dynamic "fixed_response" {
      for_each = each.value.action_type == "fixed-response" ? { 0 = 0 } : {}

      content {
        content_type = each.value.fixed_response_content_type
        message_body = each.value.fixed_response_message_body
        status_code  = each.value.fixed_response_status_code
      }
    }

    # Because of AWS provider idempotency issue, we can't use the forward block to setup a single target group
    target_group_arn = length(coalesce(each.value.target_groups, toset([]))) == 1 ? tolist(each.value.target_groups)[0].key != null ? aws_lb_target_group.this[tolist(each.value.target_groups)[0].key].arn : tolist(each.value.target_groups)[0].arn : null

    dynamic "forward" {
      for_each = each.value.action_type == "forward" && length(coalesce(each.value.target_groups, toset([]))) > 1 ? { 0 = 0 } : {}

      content {
        dynamic "target_group" {
          for_each = coalesce(each.value.target_groups, toset([]))
          content {
            arn    = target_group.value.key != null ? aws_lb_target_group.this[target_group.value.key].arn : target_group.value.arn
            weight = target_group.value.weight
          }
        }

        dynamic "stickiness" {
          for_each = coalesce(each.value.stickiness_duration, -1) != -1 ? { 0 = 0 } : {}

          content {
            duration = each.value.stickiness_duration
            enabled  = true
          }
        }
      }
    }

    dynamic "redirect" {
      for_each = each.value.action_type == "redirect" ? { 0 = 0 } : {}

      content {
        status_code = each.value.redirect_status_code

        host     = each.value.redirect_host
        path     = each.value.redirect_path
        port     = each.value.redirect_port
        protocol = each.value.redirect_protocol
        query    = each.value.redirect_query
      }
    }
  }

  lifecycle {
    precondition {
      condition     = !contains([for target_group in coalesce(each.value.target_groups, toset([])) : ((target_group.key == null && target_group.arn == null) || (target_group.key != null && target_group.arn != null))], true)
      error_message = "Listener “${each.key}”: you must set exclusively one of “arn” or “key” in target_groups objects in “var.alb_listener_rules”"
    }
  }
}

####
# Outputs
####

output "aws_lb_listeners" {
  value = merge(
    aws_lb_listener.non_alb,
    aws_lb_listener.alb,
  )
}

output "aws_lb_listener_rules" {
  value = var.alb_listeners != {} ? aws_lb_listener_rule.alb : null
}

output "aws_lb" {
  value = aws_lb.this["0"]
}
